FROM ubuntu
RUN apt-get update && apt-get install -y python python3-pip python-dev

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip3 install -r requirements.txt

COPY . /app

ENTRYPOINT ["python"]

CMD [app.py]